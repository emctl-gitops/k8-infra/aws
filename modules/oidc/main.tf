provider "aws" {
  region = var.region
}

module "gitlab_oidc" {
  source  = "saidsef/gitlab-oidc/aws"
  version = ">= 1"

  attach_read_only_policy = true
  gitlab_organisation     = var.gitlab_organisation
  gitlab_repositories     = var.gitlab_repositories
  tags                    = var.tags
  attach_admin_policy     = true
}