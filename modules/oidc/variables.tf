variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
  nullable    = false
  
}

variable "gitlab_organisation" {
  description = "GitLab Organisation"
  type        = string
  default     = "my-org"
  nullable    = false
}

variable "gitlab_repositories"  { 
    type = list(object({
    name     = string
    refs     = list(string)
    ref_type = string
  }))
  default = [
    {
      name     = ""
      refs     = []
      ref_type = ""
    }
  ]
  description = "List of GitLab repositories and refs"
}

variable "tags" {
  type = map(string)
  default = {}
  description = "Tags to apply to resources"
}