region = "us-east-1"
gitlab_organisation = "emctl-gitops"
gitlab_repositories = [
  {
    name     = "k8-infra/aws"
    refs     = ["*"]
    ref_type = "branch"
  }
]
tags = {
  "Terraform" = "true"
}