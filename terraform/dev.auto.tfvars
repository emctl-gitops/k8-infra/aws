name                           = "terraform-test"
region                         = "us-east-1"
cluster_version                = "1.32"
cluster_endpoint_public_access = true
vpc_cidr                       = "10.0.0.0/16"

tags = {
  Test       = "tf-test"
  GithubRepo = "terraform-aws-eks"
  GithubOrg  = "terraform-aws-modules"
}